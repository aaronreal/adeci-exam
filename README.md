# ADEC Innovations Assessment Exam

## Live Demo

[CLICK HERE](https://aaronreal.gitlab.io/adeci-exam/)

## Tech
- Bootstrap
- SASS
- FontAwesome
- Gulp
- jQuery